<?php


namespace Dojo\Container;

use Dojo\Container\Definition\DefinitionFactory;
use Dojo\Container\Definition\DefinitionFactoryInterface;
use Dojo\Container\Definition\DefinitionInterface;
use Dojo\Container\Exception\NotFoundException;
use Dojo\Container\Inflector\InflectorAggregate;
use Dojo\Container\Inflector\InflectorAggregateInterface;
use Dojo\Container\ServiceProvider\ServiceProviderAggregate;
use Dojo\Container\ServiceProvider\ServiceProviderAggregateInterface;

/**
 * Inversion of control container.
 *
 * @package Dojo\Container
 */
class Container implements ContainerInterface
{
    /**
     * @var \Dojo\Container\Definition\DefinitionFactoryInterface
     */
    protected $definitionFactory;

    /**
     * @var \Dojo\Container\Definition\DefinitionInterface[]
     */
    protected $definitions = [];

    /**
     * @var \Dojo\Container\ImmutableContainerInterface[]
     */
    protected $delegates = [];

    /**
     * @var \Dojo\Container\Inflector\InflectorAggregateInterface
     */
    protected $inflectors;

    /**
     * @var \Dojo\Container\ServiceProvider\ServiceProviderAggregateInterface
     */
    protected $providers;

    /**
     * @var array
     */
    protected $shared = [];

    /**
     * @var \Dojo\Container\Definition\DefinitionInterface[]
     */
    protected $sharedDefinitions = [];

    /**
     * Class constructor.
     *
     * @param \Dojo\Container\ServiceProvider\ServiceProviderAggregateInterface|null $providers
     * @param \Dojo\Container\Inflector\InflectorAggregateInterface|null $inflectors
     * @param \Dojo\Container\Definition\DefinitionFactoryInterface|null $definitionFactory
     */
    public function __construct(
        ServiceProviderAggregateInterface $providers = null,
        InflectorAggregateInterface $inflectors = null,
        DefinitionFactoryInterface $definitionFactory = null
    ) {
        $this->providers = (is_null($providers))
            ? (new ServiceProviderAggregate())->setContainer($this)
            : $providers->setContainer($this);

        $this->inflectors = (is_null($inflectors))
            ? (new InflectorAggregate())->setContainer($this)
            : $inflectors->setContainer($this);

        $this->definitionFactory = (is_null($definitionFactory))
            ? (new DefinitionFactory())->setContainer($this)
            : $definitionFactory->setContainer($this);
    }

    /**
     * Adds an item to the container.
     *
     * @param string $alias
     * @param mixed|null $concrete
     * @param boolean $share
     * @return \Dojo\Container\Definition\DefinitionInterface
     */
    public function add($alias, $concrete = null, $share = false)
    {
        if (is_null($concrete)) {
            $concrete = $alias;
        }

        $definition = $this->definitionFactory->getDefinition($alias, $concrete);

        if ($definition instanceof DefinitionInterface) {
            if ($share === false) {
                $this->definitions[$alias] = $definition;
            } else {
                $this->sharedDefinitions[$alias] = $definition;
            }

            return $definition;
        }

        // Dealing with a value that cannot build a definition.
        $this->shared[$alias] = $concrete;
    }

    /**
     * Adds a service provider to the container.
     *
     * @param string|\Dojo\Container\ServiceProvider\ServiceProviderInterface $provider
     * @return void
     */
    public function addServiceProvider($provider)
    {
        $this->providers->add($provider);
        return $this;
    }

    /**
     * Invoke a callable via the container.
     *
     * @param  callable $callable
     * @param  array $args
     * @return mixed
     */
    public function call(callable $callable, array $args = [])
    {
        return (new ReflectionContainer)->setContainer($this)->call($callable, $args);
    }

    /**
     * Delegate a backup container to be checked for services if it
     * cannot be resolved via this container.
     *
     * @param \Dojo\Container\ImmutableContainerInterface $container The container.
     * @return $this
     */
    public function delegate(ImmutableContainerInterface $container)
    {
        $this->delegates[] = $container;
        if ($container instanceof ImmutableContainerAwareInterface) {
            $container->setContainer($this);
        }

        return $this;
    }

    /**
     * Returns a definition of an item to be extended.
     *
     * @param string $alias
     * @return \Dojo\Container\Definition\DefinitionInterface
     */
    public function extend($alias)
    {
        if ($this->providers->provides($alias)) {
            $this->providers->register($alias);
        }

        if (array_key_exists($alias, $this->definitions)) {
            return $this->definitions[$alias];
        }

        if (array_key_exists($alias, $this->sharedDefinitions)) {
            return $this->sharedDefinitions[$alias];
        }

        throw new NotFoundException(
            sprintf('Unable to extend alias (%s) as it is not being managed as a definition', $alias)
        );
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $alias Identifier of the entry to look for.
     * @param array $arguments Arguments of the entry to look for.
     * @return mixed
     */
    public function get($alias, array $arguments = [])
    {
        $service = $this->getFromThisContainer($alias, $arguments);

        if ($service === false && $this->providers->provides($alias)) {
            $this->providers->register($alias);
            $service = $this->getFromThisContainer($alias, $arguments);
        }

        if ($service !== false) {
            return $service;
        }

        if ($resolved = $this->getFromDelegate($alias, $arguments)) {
            return $this->inflectors->inflect($resolved);
        }

        throw new NotFoundException(
            sprintf('Alias (%s) is not being managed by the container', $alias)
        );
    }

    /**
     * Attempt to get a service from the stack of delegated backup containers.
     *
     * @param string $alias The alias.
     * @param array $arguments The arguments.
     * @return mixed
     */
    protected function getFromDelegate($alias, array $arguments = [])
    {
        foreach ($this->delegates as $container) {
            if ($container->has($alias)) {
                return $container->get($alias, $arguments);
            }

            continue;
        }

        return false;
    }

    /**
     * Get a service that has been registered in this container.
     *
     * @param string $alias The alias.
     * @param array $arguments The arguments.
     * @return mixed
     */
    protected function getFromThisContainer($alias, array $arguments = [])
    {
        if ($this->hasShared($alias, true)) {
            return $this->inflectors->inflect($this->shared[$alias]);
        }

        if (array_key_exists($alias, $this->sharedDefinitions)) {
            $shared = $this->inflectors->inflect($this->sharedDefinitions[$alias]->build());
            $this->shared[$alias] = $shared;
            return $shared;
        }

        if (array_key_exists($alias, $this->definitions)) {
            return $this->inflectors->inflect($this->definitions[$alias]->build($arguments));
        }

        return false;
    }

    /**
     * Returns true if the container can return an entry for the given identifier. Returns false otherwise.
     *
     * Note: `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * Note: It does however mean that `get($id)` will not throw a `NotFoundException`.
     *
     * @param string $alias Identifier of the entry to look for.
     * @return boolean
     */
    public function has($alias)
    {
        if (array_key_exists($alias, $this->definitions) || $this->hasShared($alias)) {
            return true;
        }

        if ($this->providers->provides($alias)) {
            return true;
        }

        return $this->hasInDelegate($alias);
    }

    /**
     * Returns true if service is registered in one of the delegated backup containers.
     *
     * @param string $alias The alias.
     * @return boolean
     */
    protected function hasInDelegate($alias)
    {
        foreach ($this->delegates as $container) {
            if ($container->has($alias)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Returns a boolean to determine if the container has a shared instance of an alias.
     *
     * @param string $alias The alias.
     * @param boolean $resolved Whether the class is resolved?
     * @return boolean
     */
    protected function hasShared($alias, $resolved = false)
    {
        $shared = ($resolved === false) ? array_merge($this->shared, $this->sharedDefinitions) : $this->shared;
        return (array_key_exists($alias, $shared));
    }

    /**
     * Allows for manipulation of specific types on resolution.
     *
     * @param  string $type
     * @param  callable|null $callback
     * @return \Dojo\Container\Inflector\Inflector|void
     */
    public function inflector($type, callable $callback = null)
    {
        return $this->inflectors->add($type, $callback);
    }

    /**
     * Convenience method to add an item to the container as a shared item.
     *
     * @param string $alias
     * @param mixed|null $concrete
     * @return \Dojo\Container\Definition\DefinitionInterface
     */
    public function share($alias, $concrete = null)
    {
        return $this->add($alias, $concrete, true);
    }
}
