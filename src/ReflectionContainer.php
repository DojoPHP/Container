<?php


namespace Dojo\Container;

use Dojo\Container\Argument\ArgumentResolverInterface;
use Dojo\Container\Argument\ArgumentResolverTrait;
use Dojo\Container\Exception\NotFoundException;

/**
 * Class reflection container.
 *
 * @package Dojo\Container
 */
class ReflectionContainer implements ArgumentResolverInterface, ImmutableContainerInterface
{
    use ArgumentResolverTrait;
    use ImmutableContainerAwareTrait;

    /**
     * Invoke a callable via the container.
     *
     * @param callable $callable The callable.
     * @param array $arguments The arguments.
     * @return mixed
     */
    public function call(callable $callable, array $arguments = [])
    {
        if (is_string($callable) && strpos($callable, '::') !== false) {
            $callable = explode('::', $callable);
        }

        if (is_array($callable)) {
            $reflection = new \ReflectionMethod($callable[0], $callable[1]);

            if ($reflection->isStatic()) {
                $callable[0] = null;
            }

            return $reflection->invokeArgs($callable[0], $this->reflectArguments($reflection, $arguments));
        }

        if (is_object($callable)) {
            $reflection = new \ReflectionMethod($callable, '__invoke');
            return $reflection->invokeArgs($callable, $this->reflectArguments($reflection, $arguments));
        }

        $reflection = new \ReflectionFunction($callable);

        return $reflection->invokeArgs($this->reflectArguments($reflection, $arguments));
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $alias Identifier of the entry to look for.
     * @param array $arguments Arguments of the entry to look for.
     * @return mixed
     */
    public function get($alias, array $arguments = [])
    {
        if (!$this->has($alias)) {
            throw new NotFoundException(
                sprintf('Alias (%s) is not an existing class and therefore cannot be resolved', $alias)
            );
        }

        $reflector = new \ReflectionClass($alias);
        $construct = $reflector->getConstructor();

        if ($construct === null) {
            return new $alias;
        }

        return $reflector->newInstanceArgs(
            $this->reflectArguments($construct, $arguments)
        );
    }

    /**
     * Returns true if the container can return an entry for the given identifier. Returns false otherwise.
     *
     * Note: `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * Note: It does however mean that `get($id)` will not throw a `NotFoundException`.
     *
     * @param string $alias Identifier of the entry to look for.
     * @return boolean
     */
    public function has($alias)
    {
        return class_exists($alias);
    }
}
