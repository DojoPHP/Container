<?php

namespace Dojo\Container;

/**
 * Immutable inversion of control container aware interface.
 *
 * @package Dojo\Container
 */
interface ImmutableContainerAwareInterface
{
    /**
     * Returns the container.
     *
     * @return \Dojo\Container\ImmutableContainerInterface
     */
    public function getContainer();

    /**
     * Sets a container.
     *
     * @param \Dojo\Container\ImmutableContainerInterface $container
     * @return $this
     */
    public function setContainer(ImmutableContainerInterface $container);
}
