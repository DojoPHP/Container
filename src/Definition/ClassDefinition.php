<?php


namespace Dojo\Container\Definition;

/**
 * Class definition.
 *
 * @package Dojo\Container\Definition
 */
class ClassDefinition extends AbstractDefinition implements ClassDefinitionInterface
{
    /**
     * The methods used by the class definition.
     * @var array
     */
    protected $methods = [];

    /**
     * Handles the instantiation and manipulation of value and return.
     *
     * @param array $arguments The arguments.
     * @return mixed
     */
    public function build(array $arguments = [])
    {
        $arguments = (empty($arguments)) ? $this->arguments : $arguments;
        $resolved = $this->resolveArguments($arguments);
        $reflection = new \ReflectionClass($this->concrete);
        $instance = $reflection->newInstanceArgs($resolved);

        return $this->invokeMethods($instance);
    }

    /**
     * Invoke methods on resolved instance.
     *
     * @param object $instance The resolved instance.
     * @return object
     */
    public function invokeMethods($instance)
    {
        foreach ($this->methods as $method) {
            $args = $this->resolveArguments($method['arguments']);
            call_user_func_array([$instance, $method['method']], $args);
        }

        return $instance;
    }

    /**
     * Adds a method to be invoked
     *
     * @param string $method The method to be invoked.
     * @param array $arguments The arguments to be invoked.
     * @return $this
     */
    public function withMethodCall($method, array $arguments = [])
    {
        $this->methods[] = [
            'method' => $method,
            'arguments' => $arguments
        ];

        return $this;
    }

    /**
     * Adds multiple methods to be invoked
     *
     * @param array $methods The methods to be invoked.
     * @return $this
     */
    public function withMethodCalls(array $methods = [])
    {
        foreach ($methods as $method => $arguments) {
            $this->withMethodCall($method, $arguments);
        }

        return $this;
    }
}
