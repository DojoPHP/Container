<?php


namespace Dojo\Container\Definition;

use Dojo\Container\ContainerAwareInterface;

/**
 * Definition factory interface.
 *
 * @package Dojo\Container\Definition
 */
interface DefinitionFactoryInterface extends ContainerAwareInterface
{
    /**
     * Return a definition based on type of concrete.
     *
     * @param string $alias The alias.
     * @param mixed $concrete The concrete class.
     * @return mixed
     */
    public function getDefinition($alias, $concrete);
}
