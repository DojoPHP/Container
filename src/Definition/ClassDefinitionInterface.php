<?php

namespace Dojo\Container\Definition;

/**
 * Class definition interface.
 *
 * @package Dojo\Container\Definition
 */
interface ClassDefinitionInterface
{
    /**
     * Adds a method to be invoked
     *
     * @param string $method The method to be invoked.
     * @param array $arguments The arguments to be invoked.
     * @return $this
     */
    public function withMethodCall($method, array $arguments = []);

    /**
     * Adds multiple methods to be invoked
     *
     * @param array $methods The methods to be invoked.
     * @return $this
     */
    public function withMethodCalls(array $methods = []);
}
