<?php


namespace Dojo\Container\Definition;

/**
 * Callable definition.
 *
 * @package Dojo\Container\Definition
 */
class CallableDefinition extends AbstractDefinition
{
    /**
     * Handles the instantiation and manipulation of value and return.
     *
     * @param array $arguments The arguments.
     * @return mixed
     */
    public function build(array $arguments = [])
    {
        $arguments = (empty($arguments)) ? $this->arguments : $arguments;
        $resolved = $this->resolveArguments($arguments);

        if (is_array($this->concrete) && is_string($this->concrete[0])) {
            $this->concrete[0] = ($this->getContainer()->has($this->concrete[0]))
                ? $this->getContainer()->get($this->concrete[0])
                : $this->concrete[0];
        }

        return call_user_func_array($this->concrete, $resolved);
    }
}
