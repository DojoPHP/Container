<?php


namespace Dojo\Container\Definition;

use Dojo\Container\ImmutableContainerAwareTrait;

class DefinitionFactory implements DefinitionFactoryInterface
{
    use ImmutableContainerAwareTrait;

    /**
     * Return a definition based on type of concrete.
     *
     * @param string $alias The alias.
     * @param mixed $concrete The concrete class.
     * @return mixed
     */
    public function getDefinition($alias, $concrete)
    {
        if (is_callable($concrete)) {
            return (new CallableDefinition($alias, $concrete))->setContainer($this->getContainer());
        }

        if (is_string($concrete) && class_exists($concrete)) {
            return (new ClassDefinition($alias, $concrete))->setContainer($this->getContainer());
        }

        // If the item is not defineable we just return the value to be stored in the container as an arbitrary
        // value/instance.
        return $concrete;
    }
}
