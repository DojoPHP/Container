<?php

namespace Dojo\Container\Definition;

use Dojo\Container\Argument\ArgumentResolverInterface;
use Dojo\Container\Argument\ArgumentResolverTrait;
use Dojo\Container\ImmutableContainerAwareTrait;

/**
 * Abstract definition.
 *
 * @package Dojo\Container\Definition
 */
abstract class AbstractDefinition implements ArgumentResolverInterface, DefinitionInterface
{
    use ArgumentResolverTrait;
    use ImmutableContainerAwareTrait;

    /**
     * Definition alias.
     *
     * @var string
     */
    protected $alias;

    /**
     * Definition concrete class.
     *
     * @var mixed
     */
    protected $concrete;

    /**
     * Definition arguments.
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * Class constructor.
     *
     * @param string $alias The alias.
     * @param mixed $concrete The concrete class.
     */
    public function __construct($alias, $concrete)
    {
        $this->alias = $alias;
        $this->concrete = $concrete;
    }

    /**
     * Adds an argument to be injected.
     *
     * @param mixed $argument The argument to be injected.
     * @return $this
     */
    public function withArgument($argument)
    {
        $this->arguments[] = $argument;
        return $this;
    }

    /**
     * Adds multiple arguments to be injected.
     *
     * @param array $arguments The arguments to be injected.
     * @return $this
     */
    public function withArguments(array $arguments)
    {
        foreach ($arguments as $arg) {
            $this->withArgument($arg);
        }

        return $this;
    }
}
