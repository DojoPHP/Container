<?php

namespace Dojo\Container\Definition;

/**
 * Definition interface.
 *
 * @package Dojo\Container\Definition
 */
interface DefinitionInterface
{
    /**
     * Handles the instantiation and manipulation of value and return.
     *
     * @param array $arguments The arguments.
     * @return mixed
     */
    public function build(array $arguments = []);

    /**
     * Adds an argument to be injected.
     *
     * @param mixed $argument The argument to be injected.
     * @return $this
     */
    public function withArgument($argument);

    /**
     * Adds multiple arguments to be injected.
     *
     * @param array $arguments The arguments to be injected.
     * @return $this
     */
    public function withArguments(array $arguments);
}
