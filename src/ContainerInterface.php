<?php


namespace Dojo\Container;

/**
 * Inversion of control container interface.
 *
 * @package Dojo\Container
 */
interface ContainerInterface extends ImmutableContainerInterface
{
    /**
     * Adds an item to the container.
     *
     * @param string $alias
     * @param mixed|null $concrete
     * @param boolean $share
     * @return \Dojo\Container\Definition\DefinitionInterface
     */
    public function add($alias, $concrete = null, $share = false);

    /**
     * Adds a service provider to the container.
     *
     * @param string|\Dojo\Container\ServiceProvider\ServiceProviderInterface $provider
     * @return void
     */
    public function addServiceProvider($provider);

    /**
     * Invoke a callable via the container.
     *
     * @param  callable $callable
     * @param  array $args
     * @return mixed
     */
    public function call(callable $callable, array $args = []);

    /**
     * Returns a definition of an item to be extended.
     *
     * @param string $alias
     * @return \Dojo\Container\Definition\DefinitionInterface
     */
    public function extend($alias);

    /**
     * Allows for manipulation of specific types on resolution.
     *
     * @param  string $type
     * @param  callable|null $callback
     * @return \Dojo\Container\Inflector\Inflector|void
     */
    public function inflector($type, callable $callback = null);

    /**
     * Convenience method to add an item to the container as a shared item.
     *
     * @param string $alias
     * @param mixed|null $concrete
     * @return \Dojo\Container\Definition\DefinitionInterface
     */
    public function share($alias, $concrete = null);
}
