<?php

namespace Dojo\Container;

/**
 * Inversion of control container trait.
 *
 * @package Dojo\Container
 */
trait ContainerAwareTrait
{
    /**
     * Stores the container instance.
     *
     * @var \Dojo\Container\ContainerInterface
     */
    protected $container;

    /**
     * Returns the container.
     *
     * @return \Dojo\Container\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Sets a container.
     *
     * @param \Dojo\Container\ContainerInterface $container
     * @return $this
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }
}
