<?php

namespace Dojo\Container;

/**
 * Inversion of control container aware interface.
 *
 * @package Dojo\Container
 */
interface ContainerAwareInterface
{
    /**
     * Returns the container.
     *
     * @return \Dojo\Container\ContainerInterface
     */
    public function getContainer();

    /**
     * Sets a container.
     *
     * @param \Dojo\Container\ContainerInterface $container
     * @return $this
     */
    public function setContainer(ContainerInterface $container);
}
