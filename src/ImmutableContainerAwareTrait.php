<?php


namespace Dojo\Container;

/**
 * Immutable inversion of control container aware trait.
 *
 * @package Dojo\Container
 */
trait ImmutableContainerAwareTrait
{
    /**
     * Stores the container instance.
     *
     * @var \Dojo\Container\ImmutableContainerInterface
     */
    protected $container;

    /**
     * Returns the container.
     *
     * @return \Dojo\Container\ImmutableContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Sets a container.
     *
     * @param \Dojo\Container\ImmutableContainerInterface $container
     * @return $this
     */
    public function setContainer(ImmutableContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }
}
