<?php

namespace Dojo\Container;

/**
 * Generic immutable inversion of control container interface.
 *
 * @package Dojo\Container
 */
interface ImmutableContainerInterface
{
    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $alias Identifier of the entry to look for.
     * @param array $arguments Arguments of the entry to look for.
     * @return mixed
     */
    public function get($alias, array $arguments = []);

    /**
     * Returns true if the container can return an entry for the given identifier. Returns false otherwise.
     *
     * Note: `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * Note: It does however mean that `get($id)` will not throw a `NotFoundException`.
     *
     * @param string $alias Identifier of the entry to look for.
     * @return boolean
     */
    public function has($alias);
}
