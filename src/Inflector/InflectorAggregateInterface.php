<?php


namespace Dojo\Container\Inflector;

use Dojo\Container\ImmutableContainerAwareInterface;
use Dojo\Container\ImmutableContainerAwareTrait;

/**
 * Inflector aggregate interface.
 *
 * @package Dojo\Container\Inflector
 */
interface InflectorAggregateInterface extends ImmutableContainerAwareInterface
{
    /**
     * Add an inflector to the aggregate.
     *
     * @param string $type The inflector type.
     * @param callable $callback The inflector callback.
     * @return \Dojo\Container\Inflector\Inflector
     */
    public function add($type, callable $callback = null);

    /**
     * Applies all inflectors to an object.
     *
     * @param object $object The object.
     * @return object
     */
    public function inflect($object);
}
