<?php


namespace Dojo\Container\Inflector;

use Dojo\Container\ImmutableContainerAwareTrait;

class InflectorAggregate implements InflectorAggregateInterface
{
    use ImmutableContainerAwareTrait;

    /**
     * Stores all the inflectors.
     *
     * @var array
     */
    protected $inflectors = [];

    /**
     * Add an inflector to the aggregate.
     *
     * @param string $type The inflector type.
     * @param callable $callback The inflector callback.
     * @return \Dojo\Container\Inflector\Inflector
     */
    public function add($type, callable $callback = null)
    {
        if (is_null($callback)) {
            $inflector = new Inflector;
            $this->inflectors[$type] = $inflector;
            return $inflector;
        }

        $this->inflectors[$type] = $callback;
    }

    /**
     * Applies all inflectors to an object.
     *
     * @param object $object The object.
     * @return object
     */
    public function inflect($object)
    {
        foreach ($this->inflectors as $type => $inflector) {
            if (!$object instanceof $type) {
                continue;
            }

            if ($inflector instanceof Inflector) {
                $inflector->setContainer($this->getContainer());
                $inflector->inflect($object);
                continue;
            }

            // must be dealing with a callable as the inflector
            call_user_func_array($inflector, [$object]);
        }

        return $object;
    }
}
