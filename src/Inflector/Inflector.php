<?php


namespace Dojo\Container\Inflector;

use Dojo\Container\Argument\ArgumentResolverInterface;
use Dojo\Container\Argument\ArgumentResolverTrait;
use Dojo\Container\ImmutableContainerAwareTrait;

/**
 * Inflector.
 *
 * @package Dojo\Container\Inflector
 */
class Inflector implements ArgumentResolverInterface
{
    use ArgumentResolverTrait;
    use ImmutableContainerAwareTrait;

    /**
     * Stores the inflector's methods.
     *
     * @var array
     */
    protected $methods = [];

    /**
     * Stores the inflector's properties.
     * @var array
     */
    protected $properties = [];

    /**
     * Apply inflections to an object.
     *
     * @param object $object The object.
     * @return void
     */
    public function inflect($object)
    {
        $properties = $this->resolveArguments(array_values($this->properties));
        $properties = array_combine(array_keys($this->properties), $properties);

        foreach ($properties as $property => $value) {
            $object->{$property} = $value;
        }

        foreach ($this->methods as $method => $args) {
            $args = $this->resolveArguments($args);
            call_user_func_array([$object, $method], $args);
        }
    }

    /**
     * Defines a method to be invoked on the subject object.
     *
     * @param string $method The method to be invoked.
     * @param array $args The arguments to be invoked.
     * @return $this
     */
    public function invokeMethod($method, array $args)
    {
        $this->methods[$method] = $args;
        return $this;
    }

    /**
     * Defines multiple methods to be invoked on the subject object.
     *
     * @param array $methods The methods to be invoked.
     * @return $this
     */
    public function invokeMethods(array $methods)
    {
        foreach ($methods as $name => $args) {
            $this->invokeMethod($name, $args);
        }

        return $this;
    }

    /**
     * Defines a property to be set on the subject object.
     *
     * @param string $property The property to be set.
     * @param mixed $value The property value to be set.
     * @return $this
     */
    public function setProperty($property, $value)
    {
        $this->properties[$property] = $value;
        return $this;
    }

    /**
     * Defines multiple properties to be set on the subject object.
     *
     * @param array $properties The properties to be set.
     * @return $this
     */
    public function setProperties(array $properties)
    {
        foreach ($properties as $property => $value) {
            $this->setProperty($property, $value);
        }

        return $this;
    }
}
