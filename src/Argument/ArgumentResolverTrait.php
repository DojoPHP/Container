<?php

namespace Dojo\Container\Argument;

use Dojo\Container\Exception\NotFoundException;
use Dojo\Container\ReflectionContainer;

/**
 * Argument resolver trait.
 *
 * @package Dojo\Container\Argument
 */
trait ArgumentResolverTrait
{
    /**
     * Returns the container.
     *
     * @return \Dojo\Container\ContainerInterface
     */
    abstract public function getContainer();

    /**
     * Resolve an array of arguments to their concrete implementations.
     *
     * @param array $arguments The arguments to resolve.
     * @return array
     */
    public function resolveArguments(array $arguments)
    {
        foreach ($arguments as &$arg) {
            if ($arg instanceof RawArgumentInterface) {
                $arg = $arg->getValue();
                continue;
            }

            if (!is_string($arg)) {
                continue;
            }

            $container = $this->getContainer();

            if (is_null($container) && $this instanceof ReflectionContainer) {
                $container = $this;
            }

            if (!is_null($container) && $container->has($arg)) {
                $arg = $container->get($arg);

                if ($arg instanceof RawArgumentInterface) {
                    $arg = $arg->getValue();
                }

                continue;
            }
        }

        return $arguments;
    }

    /**
     * Resolves the correct arguments to be passed to a method.
     *
     * @param \ReflectionFunctionAbstract $method The method to resolve.
     * @param array $arguments The arguments to resolve.
     * @return array
     */
    public function reflectArguments(\ReflectionFunctionAbstract $method, array $arguments = [])
    {
        $resolveArguments = array_map(function (\ReflectionParameter $param) use ($method, $arguments) {
            $name = $param->getName();
            $class = $param->getClass();

            if (array_key_exists($name, $arguments)) {
                return $arguments[$name];
            }

            if (!is_null($class)) {
                return $class->getName();
            }

            if ($param->isDefaultValueAvailable()) {
                return $param->getDefaultValue();
            }

            throw new NotFoundException(sprintf(
                'Unable to resolve a value for parameter (%s) in the function/method (%s)',
                $name,
                $method->getName()
            ));
        }, $method->getParameters());

        return $this->resolveArguments($resolveArguments);
    }
}
