<?php

namespace Dojo\Container\Argument;

/**
 * Raw argument.
 *
 * @package Dojo\Container\Argument
 */
class RawArgument implements RawArgumentInterface
{
    /**
     * Raw argument value.
     *
     * @var mixed
     */
    protected $value;

    /**
     * Class constructor.
     *
     * @param mixed $value Raw argument value.
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Return the value of the raw argument.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
