<?php

namespace Dojo\Container\Argument;

/**
 * Raw argument interface.
 *
 * @package Dojo\Container\Argument
 */
interface RawArgumentInterface
{
    /**
     * Return the value of the raw argument.
     *
     * @return mixed
     */
    public function getValue();
}
