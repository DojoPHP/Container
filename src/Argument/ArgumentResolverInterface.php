<?php

namespace Dojo\Container\Argument;

use Dojo\Container\ImmutableContainerAwareInterface;
use Dojo\Container\ImmutableContainerInterface;

/**
 * Argument resolver interface.
 *
 * @package Dojo\Container\Argument
 */
interface ArgumentResolverInterface extends ImmutableContainerAwareInterface
{
    /**
     * Resolve an array of arguments to their concrete implementations.
     *
     * @param array $arguments The arguments to resolve.
     * @return array
     */
    public function resolveArguments(array $arguments);

    /**
     * Resolves the correct arguments to be passed to a method.
     *
     * @param \ReflectionFunctionAbstract $method The method to resolve.
     * @param array $arguments The arguments to resolve.
     * @return array
     */
    public function reflectArguments(\ReflectionFunctionAbstract $method, array $arguments = []);
}
