<?php


namespace Dojo\Container\ServiceProvider;

use Dojo\Container\ContainerAwareTrait;

/**
 * Service provider aggregate.
 *
 * @package Dojo\Container\ServiceProvider
 */
class ServiceProviderAggregate implements ServiceProviderAggregateInterface
{
    use ContainerAwareTrait;

    /**
     * All providers.
     *
     * @var array
     */
    protected $providers = [];

    /**
     * Registered providers.
     *
     * @var array
     */
    protected $registered = [];

    /**
     * Adds a service provider to the aggregate.
     *
     * @param string|\Dojo\Container\ServiceProvider\ServiceProviderInterface $provider The provider to add.
     * @return $this
     */
    public function add($provider)
    {
        if (is_string($provider) && class_exists($provider)) {
            $provider = new $provider;
        }

        if ($provider instanceof ContainerAwareTrait) {
            $provider->setContainer($this->getContainer());
        }

        if ($provider instanceof BootableServiceProviderInterface) {
            $provider->boot();
        }

        if ($provider instanceof ServiceProviderInterface) {
            foreach ($provider->provides() as $service) {
                $this->providers[$service] = $provider;
            }

            return $this;
        }

        throw new \InvalidArgumentException(
            'A service provider must be a fully qualified class name or instance ' .
            'of (\League\Container\ServiceProvider\ServiceProviderInterface)'
        );
    }

    /**
     * Determines whether a service is provided by the aggregate.
     *
     * @param string $service The service.
     * @return boolean
     */
    public function provides($service)
    {
        return array_key_exists($service, $this->providers);
    }

    /**
     * Invokes the register method of a provider that provides a specific service.
     *
     * @param string $service The service.
     * @return void
     */
    public function register($service)
    {
        if (!array_key_exists($service, $this->providers)) {
            throw new \InvalidArgumentException(
                sprintf('(%s) is not provided by a service provider', $service)
            );
        }

        $provider = $this->providers[$service];
        $signature = get_class($provider);

        if ($provider instanceof SignatureServiceProviderInterface) {
            $signature = $provider->getSignature();
        }

        // Ensure that the provider hasn't already been invoked by any other service request
        if (in_array($signature, $this->registered)) {
            return;
        }

        /* @var \Dojo\Container\ServiceProvider\ServiceProviderInterface $provider */
        $provider->register();

        $this->registered[] = $signature;
    }
}
