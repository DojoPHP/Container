<?php


namespace Dojo\Container\ServiceProvider;

use Dojo\Container\ContainerAwareInterface;

/**
 * Service provider interface.
 *
 * @package Dojo\Container\ServiceProvider
 */
interface ServiceProviderInterface extends ContainerAwareInterface
{
    /**
     * Returns a boolean if checking whether this provider provides a specific service or returns an array of provided
     * services if no argument passed.
     *
     * @param null|string $service The service.
     * @return boolean|array
     */
    public function provides($service = null);

    /**
     * Use the register method to register items with the container via the protected $this->container property or the
     * `getContainer` method from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register();
}
