<?php


namespace Dojo\Container\ServiceProvider;

/**
 * Signature service provider interface.
 *
 * @package Dojo\Container\ServiceProvider
 */
interface SignatureServiceProviderInterface
{
    /**
     * The signature of the service provider uniquely identifies it, so that we can quickly determine if it has already
     * been registered. Defaults to get_class($provider).
     *
     * @return string
     */
    public function getSignature();

    /**
     * Set a custom signature for the service provider. This enables registering the same service provider multiple
     * times.
     *
     * @param  string $signature
     * @return $this
     */
    public function withSignature($signature);
}
