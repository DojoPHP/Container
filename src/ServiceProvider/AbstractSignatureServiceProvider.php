<?php


namespace Dojo\Container\ServiceProvider;

/**
 * Abstract signature service provider.
 *
 * @package Dojo\Container\ServiceProvider
 */
abstract class AbstractSignatureServiceProvider
    extends AbstractServiceProvider
    implements SignatureServiceProviderInterface
{
    /**
     * Stores the signature.
     *
     * @var string
     */
    protected $signature;

    /**
     * The signature of the service provider uniquely identifies it, so that we can quickly determine if it has already
     * been registered. Defaults to get_class($provider).
     *
     * @return string
     */
    public function getSignature()
    {
        return (is_null($this->signature)) ? get_class($this) : $this->signature;
    }

    /**
     * Set a custom signature for the service provider. This enables registering the same service provider multiple
     * times.
     *
     * @param  string $signature
     * @return $this
     */
    public function withSignature($signature)
    {
        $this->signature = $signature;
        return $this;
    }
}
