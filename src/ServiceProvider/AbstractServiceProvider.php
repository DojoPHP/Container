<?php


namespace Dojo\Container\ServiceProvider;

use Dojo\Container\ContainerAwareTrait;

/**
 * Abstract service provider.
 *
 * @package Dojo\Container\ServiceProvider
 */
abstract class AbstractServiceProvider implements ServiceProviderInterface
{
    use ContainerAwareTrait;

    /**
     * @var array
     */
    protected $provides = [];

    /**
     * Returns a boolean if checking whether this provider provides a specific service or returns an array of provided
     * services if no argument passed.
     *
     * @param null|string $service The service.
     * @return boolean|array
     */
    public function provides($service = null)
    {
        if (!is_null($service)) {
            return (in_array($service, $this->provides));
        }

        return $this->provides;
    }
}
