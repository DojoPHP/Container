<?php


namespace Dojo\Container\ServiceProvider;

use Dojo\Container\ContainerAwareInterface;

/**
 * Service provider aggregate interface.
 *
 * @package Dojo\Container\ServiceProvider
 */
interface ServiceProviderAggregateInterface extends ContainerAwareInterface
{
    /**
     * Adds a service provider to the aggregate.
     *
     * @param string|\Dojo\Container\ServiceProvider\ServiceProviderInterface $provider The provider to add.
     * @return $this
     */
    public function add($provider);

    /**
     * Determines whether a service is provided by the aggregate.
     *
     * @param string $service The service.
     * @return boolean
     */
    public function provides($service);

    /**
     * Invokes the register method of a provider that provides a specific service.
     *
     * @param string $service The service.
     * @return void
     */
    public function register($service);
}
