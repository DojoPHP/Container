<?php

namespace Dojo\Container\Exception;

/**
 * Exception that will be thrown when an entry is not found.
 *
 * @package Dojo\Container\Exception
 */
class NotFoundException extends \InvalidArgumentException implements ContainerException
{
    // Do nothing for now...
}
