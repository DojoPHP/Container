<?php

namespace Dojo\Container\Exception;

/**
 * Generic container exception interface.
 *
 * @package Dojo\Container\Exception
 */
interface ContainerException
{
    // Do nothing for now...
}